/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Hilman Taris M
 */
public class Table {
    
    public static void setHeaderTable(JTable table) {
        JTableHeader header = table.getTableHeader();
        header.setOpaque(false);
        header.setBackground(new Color(54, 54, 54));
        header.setForeground(new Color(255, 255, 255));
        header.setFont(new Font("Raleway ExtraBold", Font.PLAIN, 18));
        header.setPreferredSize(new Dimension(200, 70));
        table.setRowHeight(44);
    }
    
}
