/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.awt.Color;

/**
 *
 * @author Hilman Taris M
 */
public class SideBar {
    
    private final static Color mouseHover = new Color(112,112,112);
    private final static Color mouseExit = new Color(65,65,65);
    
    public static Color getMouseHoverColor() {
        return mouseHover;
    }
    
    public static Color getMouseExitColor() {
        return mouseExit;
    }
    
}
