/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;
    

public class Guru {
    private Integer NIP;
    private String Nama;
    private String Email;
    private String No_Telp;
    private Integer Status;
    private String Alamat;
    private Integer Jenkel;
    private String Matpel;
    private String Barcode;
    private String Created_at;
    private String Updated_at;
 
    public Integer getNIP(){
        return this.NIP;
    }
    public void setNIP(){
        this.NIP = NIP;
    }
    public String getNama(){
        return this.Nama;
    }
    public void setNama(){
        this.Nama = Nama;
    }
    public String getEmail(){
        return this.Email;
    }
    public void setEmail(){
        this.Email = Email;
    }
    public String getNo_Telp(){
        return this.No_Telp;
    }
    public void setNo_Telp(){
        this.No_Telp = No_Telp;
    }
    public Integer getStatus(){
        return this.Status;
    }
    public void setStatus(){
        this.Status = Status;
    }
    public String getAlamat(){
        return this.Alamat;
    }
    public void setAlamat(){
        this.Alamat = Alamat;
    }
    public Integer getJenkel(){
        return this.Jenkel;
    }
    public void setJenkel(){
        this.Jenkel = Jenkel;
    }
    public String getMatpel(){
        return this.Matpel;
    }
    public void setMatpel(){
        this.Matpel = Matpel;
    }
    public String getBarcode(){
        return this.Barcode;
    }
    public void setBarcode(){
        this.Barcode = Barcode;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String Created_at) {
        this.Created_at = Created_at;
    }

    public String getUpdated_at() {
        return Updated_at;
    }

    public void setUpdated_at(String Updated_at) {
        this.Updated_at = Updated_at;
    }
    
    
}
