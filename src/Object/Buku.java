/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

/**
 *
 * @author MDHN
 */
public class Buku {
    private Integer NIS;
    private String Nama;
    private String Email;
    private String No_Telp;
    private String Kelas;
    private String Jurusan;
    private String Alamat;
    private Integer Jenkel;
    private Integer Status;
    private String barcode;
    private String Created_at;
    private String Updated_at;
 
    public Integer getNIS(){
        return this.NIS;
    }
    public void setNIS(){
        this.NIS = NIS;
    }
    public String getNama(){
        return this.Nama;
    }
    public void setNama(){
        this.Nama = Nama;
    }
    public String getEmail(){
        return this.Email;
    }
    public void setEmail(){
        this.Email = Email;
    }
    public String getNo_Telp(){
        return this.No_Telp;
    }
    public void setNo_Telp(){
        this.No_Telp = No_Telp;
    }
    public String getKelas(){
        return this.Kelas;
    }
    public void setKelas(){
        this.Kelas = Kelas;
    }
    public String getJurusan(){
        return this.Jurusan;
    }
    public void setJurusan(){
        this.Jurusan = Jurusan;
    }
    public String getAlamat(){
        return this.Alamat;
    }
    public void setAlamat(){
        this.Alamat = Alamat;
    }
    public Integer getJenkel(){
        return this.Jenkel;
    }
    public void setJenkel(){
        this.Jenkel = Jenkel;
    }
    public Integer getStatus(){
        return this.Status;
    }
    public void setStatus(){
        this.Status = Status;
    }
    public String getbarcode(){
        return this.barcode;
    }
    public void setbarcode(){
        this.barcode = barcode;
    }
    
}
