/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.auth;

import Connection.Connect;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Hilman Taris M
 */
public class Authentication {
    
    Connection koneksi;
    private String username = null, password = null;
    
    public Authentication(String username, String password) {
        koneksi = Connect.getKoneksi();
        this.username = username;
        this.password = password;
    }
    
    public Boolean compare() {
        Boolean res = null;
        
        try {
            Statement statement = koneksi.createStatement();
            String query = "SELECT username, password FROM t_admin";
            ResultSet result = statement.executeQuery(query);
            result.next();
            String username = result.getString("username");
            String hashPass = result.getString("password");
            
            PassHash hash = new PassHash();
            String passEncrypt = hash.encrypt(password);
            
            res = (hashPass.equals(passEncrypt) && this.username.equals(username)) ? true : false;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return res;
    }
    
}
