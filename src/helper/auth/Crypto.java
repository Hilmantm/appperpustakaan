/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.auth;

/**
 *
 * @author Hilman Taris M
 */
public interface Crypto {
    
    String encrypt(String Data);
    
    String decrypt(String Data);
    
}
