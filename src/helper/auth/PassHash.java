/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.auth;

import java.security.Key;
import java.util.ArrayList;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Hilman Taris M
 */
public class PassHash implements Crypto {
    
    public static final String ALGORYTHM = "AES";
    
    private byte[] keyValue;
    
    public PassHash() {
        this.keyValue = Data.getKey().getBytes();
    }

    @Override
    public String encrypt(String Data) {
        String encryptedValue = null;
        try {
            Key key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORYTHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptValue = cipher.doFinal(Data.getBytes());
            encryptedValue = new BASE64Encoder().encode(encryptValue);
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return encryptedValue;
    }

    @Override
    public String decrypt(String Data) {
        String decryptValue = null;
        try {
            Key key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORYTHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decoderValue = new BASE64Decoder().decodeBuffer(Data);
            byte[] decValue = cipher.doFinal(decoderValue);
            decryptValue = new String(decValue);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return decryptValue;
    }
    
    public Key generateKey() {
        Key key = new SecretKeySpec(keyValue, ALGORYTHM);
        return key;
    }
    
}
